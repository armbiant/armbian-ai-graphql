use crate::node::Node;
use mcai_types::Coordinates;
use std::cell::Ref;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LinkType {
  Parentage,
  Requirement,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Link {
  start_node_id: u32,
  end_node_id: u32,
  start: Coordinates,
  end: Coordinates,
  kind: LinkType,
}

impl Link {
  pub fn new(
    start_node_id: u32,
    end_node_id: u32,
    start: Coordinates,
    end: Coordinates,
    kind: LinkType,
  ) -> Self {
    Self {
      start_node_id,
      end_node_id,
      start,
      end,
      kind,
    }
  }

  pub fn start_node_id(&self) -> u32 {
    self.start_node_id
  }

  pub fn end_node_id(&self) -> u32 {
    self.end_node_id
  }

  pub fn start(&self) -> Coordinates {
    self.start.clone()
  }

  pub fn end(&self) -> Coordinates {
    self.end.clone()
  }

  pub fn kind(&self) -> LinkType {
    self.kind
  }
}

impl From<(&Node, Ref<'_, Node>, LinkType)> for Link {
  fn from((from, to, kind): (&Node, Ref<Node>, LinkType)) -> Self {
    Link::new(
      from.id(),
      to.id(),
      from.get_input_coordinates(),
      to.get_output_coordinates(),
      kind,
    )
  }
}
